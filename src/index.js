import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import {Provider} from 'react-redux';
import {store} from './store';

const root = document.createElement('div');

document.body.append(root);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  , root
);
