import {createStore, compose, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
// import {reduxFirestore, getFirestore} from 'redux-firestore';
// import {reactReduxFirebase, getFirebase} from 'react-redux-firebase';
// import firebaseConfig from '../config/firebase';
import {default as reducer} from '../reducer';

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer,
  composeEnhancer(
    applyMiddleware(reduxThunk)
    // reduxFirestore(firebaseConfig),
    // reactReduxFirebase(firebaseConfig)
  )
);

