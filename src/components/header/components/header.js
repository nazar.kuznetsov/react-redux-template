import React from 'react';
import {withRouter} from 'react-router';
import {NavLink} from 'react-router-dom';

const Header = React.memo(({location}) => {

  const currentHeader = ({pathname}) => {
    switch (pathname) {
      case '/sent': return 'header header__page-sent';
      case '/trash': return 'header header__page-trash';
      case '/spam': return 'header header__page-spam';
      default: return 'header header__page-inbox';
    }
  };

  return (
    <header className={currentHeader(location)}>
      <nav className="nav">
        <ul>
          <NavLink exact={true} to="/">Входящие</NavLink>
          <NavLink exact={true} to="/sent">Отправленые</NavLink>
          <NavLink exact={true} to="/spam">Спам</NavLink>
          <NavLink exact={true} to="/trash">Корзина</NavLink>
        </ul>
      </nav>
    </header>
  );

});

export default withRouter(Header);

